# Yandex Contest REST

## Quick start

First you need to execute db.py script& You can do this inside\outside the Docker container.

```shell script
python3 db.py
sudo docker-compose build
sudo docker-compose up -d
```

## Tech stack
* Flask
* Sqllite
* Gunicorn
