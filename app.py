from flask import Flask, request, jsonify, abort
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from sqlalchemy.sql import text
from sqlalchemy import and_, or_, event, insert
from sqlalchemy.exc import IntegrityError
from sqlalchemy.engine import Engine
from sqlalchemy.orm import backref

from marshmallow import fields, ValidationError, validate, validates, validates_schema, EXCLUDE

from datetime import timedelta
import os

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
DB_URL = 'sqlite:///' + os.path.join(basedir, 'database.db')
app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


class Item(db.Model):
    __tablename__ = 'item'
    __table_args__ = (
        db.CheckConstraint(or_(text("type=='CATEGORY'"), and_(text("price>=0"), text("type=='OFFER'")))),
        db.CheckConstraint(text("id<>parentId")),
        db.ForeignKeyConstraint(["parentId"], ["item.id"], ondelete='cascade')
    )

    id = db.Column(db.String(64), primary_key=True, unique=True, nullable=False)
    name = db.Column(db.String(64), nullable=False)
    parentId = db.Column(db.String(64))
    price = db.Column(db.Integer)
    type = db.Column(db.String(8), nullable=False)
    date = db.Column(db.DATETIME, nullable=False)
    offers = db.Column(db.Integer, nullable=False)

    children = db.relationship('Item', cascade='all, delete', backref=backref("parent", remote_side='Item.id'))

    def __init__(self, id, name, type, price=None, parentId=None, date=None):
        self.id = id
        self.name = name
        self.parentId = parentId
        self.price = price
        self.type = type
        self.date = date
        self.offers = 0

    def change_parent(self, newParentId, updateDate):
        old_parent = self.parent
        # print('change_parent_and_price {} {}'.format(old_parent, self))
        if old_parent:
            old_parent.children.remove(self)
            if self.type == 'OFFER':
                if self.price != 0:
                    old_parent.update_parent_price(diff=-self.price, updateDate=updateDate)
            if self.type == 'CATEGORY':
                diff = self.price * self.offers if self.price is not None else 0
                if diff != 0:
                    old_parent.update_parent_price(diff=-diff, updateDate=updateDate)

            if newParentId is not None:
                new_parent = Item.query.filter(Item.id == newParentId).first()
                self.parent = new_parent
                if self.type == 'OFFER':
                    if self.price != 0:
                        new_parent.update_parent_price(diff=self.price, updateDate=updateDate)
                if self.type == 'CATEGORY':
                    diff = self.price * self.offers if self.price is not None else 0
                    if diff != 0:
                        new_parent.update_parent_price(diff=diff, updateDate=updateDate)

    def info(self):
        return {'id': self.id, 'name': self.name, 'parentId': self.parentId,
                'price': self.price, 'type': self.type, 'date': datetime_iso(self), 'children': []}

    def get_tree(self, dst):
        dst.append(self.info())
        if self.children:
            for child in self.children:
                child.get_tree(dst[-1]['children'])
        else:
            dst[-1]['children'] = None
            return

    def update_parent_price(self, diff, updateDate):
        parent = self.parent
        if parent and diff != 0:
            offers = sum(child.offers for child in parent.children if child.type == 'CATEGORY') + \
                     sum(1 for child in parent.children if child.type == 'OFFER')
            if offers != 0:
                par_price = parent.price if parent.price is not None else 0
                parent.price = (par_price * parent.offers + diff) / offers
            if offers == 0:
                parent.price = None
            parent.offers = offers
            parent.date = updateDate
            parent.update_parent_price(diff=diff, updateDate=updateDate)


def datetime_iso(item):
    return item.date.isoformat(timespec='milliseconds') + 'Z'


class Statistics(db.Model):
    __tablename__ = 'statistics'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, default=0)
    item_id = db.Column(db.String(64), db.ForeignKey('item.id', ondelete='cascade'), unique=False)
    name = db.Column(db.String(64), nullable=False)
    parentId = db.Column(db.String(64))
    price = db.Column(db.Integer)
    type = db.Column(db.String(8), nullable=False)
    date = db.Column(db.DATETIME, nullable=False)
    offers = db.Column(db.Integer)

    def info(self):
        return {'id': self.item_id, 'name': self.name, 'parentId': self.parentId,
                'price': self.price, 'type': self.type, 'date': datetime_iso(self)}


class DataSchemaImports(ma.Schema):
    items = fields.List(required=True, cls_or_instance=fields.Dict())
    updateDate = fields.DateTime(required=True, format="%Y-%m-%dT%H:%M:%S.%fZ")

    class Meta:
        unknown = EXCLUDE


class ItemSchemaImports(ma.Schema):
    id = fields.Str(required=True, validate=validate.Length(max=64))
    name = fields.Str(required=True, validate=validate.Length(max=64))
    type = fields.Str(required=True, validate=validate.OneOf(['OFFER', 'CATEGORY']))

    parentId = fields.Str(required=False, validate=validate.Length(max=64), allow_none=True)
    price = fields.Integer(required=False, allow_none=True)

    @validates_schema
    def preload_check(self, data, **kwargs):
        id = data.get('id')
        parentId = data.get('parentId')
        if parentId and id == parentId:
            raise ValidationError('parentId == id')

        type = data.get('type')
        price = data.get('price')
        if price is not None:
            if type == 'OFFER' and price < 0:
                raise ValidationError('type == "OFFER" and price < 0')

            if type == 'CATEGORY':
                raise ValidationError('type == "CATEGORY" and price is not None')
        else:
            if type == 'OFFER':
                raise ValidationError('type == "OFFER" and price is None')

        return data

    class Meta:
        unknown = EXCLUDE


class DataSchemaSales(ma.Schema):
    date = fields.DateTime(required=True, format="%Y-%m-%dT%H:%M:%S.%fZ")


class DataSchemaNodesStatisic(ma.Schema):
    dateStart = fields.DateTime(required=False, allow_none=True, format="%Y-%m-%dT%H:%M:%S.%fZ")
    dateEnd = fields.DateTime(required=False, allow_none=True, format="%Y-%m-%dT%H:%M:%S.%fZ")

    class Meta:
        unknown = EXCLUDE


item_schema = ItemSchemaImports()
items_schema = ItemSchemaImports(many=True)
data_schema = DataSchemaImports()

data_schema_sales = DataSchemaSales()
data_schema_nodes_stat = DataSchemaNodesStatisic()


@app.route('/imports', methods=['POST'])
def imports():
    # check that all items by marshmallow
    data = None
    items = None
    try:
        data = data_schema.load(request.json)
        items = items_schema.load(data['items'])
    except ValidationError as err:
        print(err.messages)
        abort(400)
    updateDate = data['updateDate']

    # duplicates check
    all_id = [item['id'] for item in items]
    if len(all_id) != len(set(all_id)):
        abort(400)

    # check that parentId have type CATEGORY
    parentIds = [item['parentId'] for item in items if item.get('parentId') is not None]
    parents = Item.query.filter(Item.id.in_(parentIds)).all()
    for parent in parents:
        if parent.type != 'CATEGORY':
            abort(400)

    # find all exist items in table
    exist_items = Item.query.filter(Item.id.in_(all_id)).all()
    exist_items_id = [item.id for item in exist_items]

    # add items to table
    items_to_add = list(filter(lambda x: x['id'] not in exist_items_id, items))
    new_items = [Item(**n) for n in items_to_add]
    for item in new_items:
        item.date = updateDate
        db.session.add(item)
        db.session.commit()
        if item.type == 'OFFER':
            item.update_parent_price(diff=item.price, updateDate=updateDate)
            db.session.commit()

    # update items in table
    items_to_update = list(filter(lambda x: x['id'] in exist_items_id, items))
    for eitem in exist_items:
        item = [item for item in items_to_update if item['id'] == eitem.id][0]
        items_to_update.remove(item)

        eitem.name = item.get('name')
        # updateDate > eitem.date -- Guaranteed
        eitem.date = updateDate

        old_parentId = eitem.parentId
        new_parentId = item.get('parentId')

        old_price = eitem.price
        new_price = item.get('price')

        if new_parentId != old_parentId and old_price != new_price:
            eitem.change_parent(new_parentId, updateDate)
            eitem.price = new_price
            if new_price is not None:
                eitem.update_parent_price(new_price, updateDate)

        if new_parentId != old_parentId and old_price == new_price:
            eitem.change_parent(new_parentId, updateDate)

        if old_price != new_price and new_parentId == old_parentId:
            if new_price is not None:
                diff = new_price - old_price
                eitem.update_parent_price(diff, updateDate)
                eitem.price = new_price

        db.session.commit()

    return jsonify({"code": 200, "message": "The insert or update was successful"})


@app.route('/delete/<id>', methods=['DELETE'])
def delete(id):
    item = Item.query.filter(Item.id == id).first()
    if item is not None:
        db.session.delete(item)
        if item.type == 'OFFER':
            if item.price != 0:
                updateDate = item.parent.date if item.parent is not None else None
                item.update_parent_price(diff=-item.price, updateDate=updateDate)
        if item.type == 'CATEGORY':
            diff = item.price * item.offers if item.price is not None else 0
            if diff != 0:
                updateDate = item.parent.date if item.parent is not None else None
                item.update_parent_price(diff=-diff, updateDate=updateDate)

        db.session.commit()
        return jsonify({"code": 200, "message": "The removal was successful"})
    else:
        abort(404, description="Item not found")


@app.route('/nodes/<id>', methods=['GET'])
def nodes(id):
    item = Item.query.filter(Item.id == id).first()
    if item is not None:
        dst = []
        item.get_tree(dst=dst)
        return jsonify(dst[0])
    else:
        abort(404, description="Item not found")


@app.route('/sales', methods=['GET'])
def sales():
    data = None
    try:
        data = data_schema_sales.load({'date': request.args.get('date')})
    except ValidationError as err:
        abort(400, description="Validation Failed")

    now = data.get('date')
    if now is not None:
        now24 = now - timedelta(hours=24)
        stat = Statistics.query.filter(
            and_(Statistics.date >= now24, Statistics.date <= now, Statistics.type == 'OFFER')).all()
        if stat is not None:
            d = {"items": [s.info() for s in stat]}
            return jsonify(d)
        else:
            return jsonify({"items": []})


@app.route('/node/<id>/statistic', methods=['GET'])
def nodes_statistic(id):
    dateStart = request.args.get('dateStart')
    dateEnd = request.args.get('dateEnd')
    data = None
    try:
        data = data_schema_nodes_stat.load({"dateStart": dateStart, "dateEnd": dateEnd})
    except ValidationError as err:
        abort(400, description="Validation Failed")

    dateStart = data.get('dateStart')
    dateEnd = data.get('dateEnd')
    stat = None
    if dateStart is not None and dateEnd is not None:
        stat = Statistics.query.filter(
            and_(Statistics.item_id == id, Statistics.date >= dateStart, Statistics.date < dateEnd)).all()
    if dateStart is not None and dateEnd is None:
        stat = Statistics.query.filter(
            and_(Statistics.item_id == id, Statistics.date >= dateStart)).all()
    if dateStart is None and dateEnd is not None:
        stat = Statistics.query.filter(
            and_(Statistics.item_id == id, Statistics.date < dateEnd)).all()

    if stat is not None:
        d = {"items": [s.info() for s in stat]}
        return jsonify(d)
    else:
        abort(404, description="Item not found")


if __name__ == '__main__':
    app.run(host='0.0.0.0')
