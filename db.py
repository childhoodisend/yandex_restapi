from app import db

'''
drop and create db, execute once before run app.py
'''

db.drop_all()
db.create_all()

db.engine.execute('''create trigger if not exists insert_trigger after insert on item
begin
    insert into statistics(item_id, name, parentId, price, type, date, offers) 
    VALUES (new.id, new.name, new.parentId, new.price, new.type, new.date, new.offers);
end;''')

db.engine.execute('''create trigger if not exists update_trigger after update on item
begin
    insert into statistics(item_id, name, parentId, price, type, date, offers)
    VALUES (new.id, new.name, new.parentId, new.price, new.type, new.date, new.offers);
end;''')